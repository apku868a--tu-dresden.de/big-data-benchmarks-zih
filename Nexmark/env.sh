# Framework variables

#==========================================================
# User defined variables
#==========================================================
export BENCHMARK_ROOT_DIR=/home/apku868a/benchmarks/nexmark/nexmark-benchmark
export TMP_DIR="/scratch/ws/0/apku868a-streaming/tmp_benchmark/tmp_${SLURM_JOB_ID}"

# please ensure that flink files are present at <ROOT>/flink
# please ensure that nexmark files are present at <ROOT>/nexmark

#==========================================================
# Framework internal variables
#========================================================
export NEXMARK_BENCHMARK_DIR="$BENCHMARK_ROOT_DIR"
export NEXMARK_HOME="$BENCHMARK_ROOT_DIR"/nexmark
export NEXMARK_CONF_DIR="$NEXMARK_HOME"/conf         # This shouldn't be changed
#export NEXMARK_CONF_DIR="$TMP_DIR"/conf/nexmark

export NEXMARK_LOG_DIR="$TMP_DIR"/log
export NEXMARK_CHECKPOINTS_DIR=file:///$TMP_DIR/checkpoints
export NEXMARK_ROCKSDB_DIR=$TMP_DIR/rocksDB

export FLINK_HOME=$BENCHMARK_ROOT_DIR/flink
export FLINK_BIN_DIR=$FLINK_HOME/bin
export FLINK_CONF_DIR=$FLINK_HOME/conf
#export FLINK_CONF_DIR=$TMP_DIR/conf/flink
export FLINK_LOG_DIR=$TMP_DIR/log

export PATH=$PATH:$NEXMARK_HOME/bin:$FLINK_BIN_DIR

# The idea of using different configuration from different folders
# for running multiple instances of the benchmark is abandoned. It
# done because of complexities/dependencies involved with bin/config.sh
# (flink and nexmark files) and side_input_gen.sh files.
