# Nexmark benchmark

## About the repository

---

- This folder contains scripts required to run [Nexmark benchmark](https://github.com/nexmark/nexmark) on zih systems.

## Getting started

---

### **Prerequsites**

- Flink and nexmark source code files should be present in the folder where these scripts are run.
- To run any nexmark benchmark on zih system, follow the steps until step 3 (incluing step 3) mentioned in [Nexmark official documentation](https://github.com/nexmark/nexmark).

### **Setup**

User must set benchmark root directory path (i.e. where the scripts, flink folder and nexmark folder is present) and temporary folder path (where checkpointing data and logs are stored) paths in `env.sh` in the "user defined variables" section. Other variables shouldnot be modified unless it is necessary to do so.

The folder structure should look like as follows:

```bash
└── root_dir
    ├── env.sh
    ├── benchmark_nexmark.sh
    ├── benchmark_nexmark_sbatch.sh
    ├── benchmark_nexmark_chain_job.sh
    ├── flink
    |   ├── <flink_files>
    |   └── <flink_files>
    └── nexmark
        ├── <nexmark_files>
        └── <nexmark_files>
```

### **Usage**

- All the other steps after step 3 in [Nexmark official documentation](https://github.com/nexmark/nexmark), are carried out by `benchmark_nexmark.sh` script. Please read beow for more information.
- This folder contains following files:
  - **`benchmark_nexmark.sh`**
    - Description:
      - It is the main file used to run benchmark query.
      - It can be run in either interactive or batch mode.
      - This files does all the necessary things like setting up workers, adjusting parallelism, slots, replacing configuration files etc.
    - Usage:
      - `source benchmark_nexmark.sh [run <query> <slots_per_taskmanager> <parallelism>|setup|start|stop]`
      - `benchmark_nexmark.sh -h` for more information
    - Example:
      - To run query 0 with 8 slots per taskmanager and with 8 parallelism level, use `benchmark_nexmark.sh run q0 8 8`.
      - For the setup of 2 nodes, 4 cpus per node, there are in total 8 cpus. This means that we can use maximum available paralleism=8 with slots per taskmanager=4 (i.e. cpus per node). The benchmark can be run using: `benchmark_nexmark.sh run q0 4 8`
  
  - **`benchmark_nexmark_sbatch.sh`**
    - Description:
      - It is the wrapper around main file used to run benchmark query in batch mode.
    - Usage:

        ```bash
        $ benchmark_nexmark_sbatch.sh                           \
            --partition=<slurm_partition>                       \
            --nodes=<num_nodes>                                 \
            --ntasks=<num_nodes>                                \
            --cpus-per-tasks=<num_cpu_per_node_or_per_tasks>    \
            --mem-per-cpu=<mem_per_cpu>                         \
            --time=<duration_of_benchmark run>                  \
            <query>
        ```

      - User must set project name (--account) as parameter inside the sbatch file.

    - Example:
      - For the setup of partition=haswell 2 nodes, 4 cpus per node, there are in total 8 cpus. This means that we can use maximum available paralleism=8 with slots per taskmanager=4 (i.e. cpus per node). The benchmark can be run in sbatch mode using:

        ```bash
        $ benchmark_nexmark_sbatch.sh
            --partition=haswell64 \
            --nodes=2             \
            --ntasks=2            \
            --cpus-per-tasks=4    \
            --mem-per-cpu=2G      \
            --time=1:00:00        \
            q0
        ```

  - **`benchmark_nexmark_chain_job.sh`**
    - Description:
      - This file is used to submit multiple benchmark runs that are chained with each other (the next job starts only when the previous job is finished).
      - It is the wrapper around `benchmark_nexmark_sbatc.sh` file.
      - User must set following variables inside the file:
        - PARTITION="<slurm_partition>"
        - NNODES="<list_of_number_of_nodes_seperated_by_spaces>"
        - NCPU="<list_of_number_of_cpus_per_node_seperated_by_spaces>"
        - MEM_PER_CPU="<memory_per_cpu>"
        - TIME="\<time>"
        - QUERIES="<list_of_queries_seperated_by_spaces>"
    - Usage:
      - `./benchmark_nexmark_chain_job.sh`

    - Example:
      - For the setup of 1,2,4 nodes, 2,4,8 cpus per node, the benchmark can be run with following variable values
        - PARTITION="haswell64"
        - NNODES="1 2 4"
        - NCPU="2 4 8"
        - MEM_PER_CPU="2G"
        - TIME="01:00:00"
        - QUERIES="q0 q1 q2 q3"
- Note:
  - It is recommended to use one taskmanager per node for lower communication overhead
  - It is recommended to use time > 1:00:00 hrs.

## Contributors

---

- Apurv Kulkarni (apurv.kulkarni@tu-dresden.de)
- Elias Werner (elias.werner@tu-dresden.de)
- Jan Frenzel (jan.frenzel@tu-dresden.de)
