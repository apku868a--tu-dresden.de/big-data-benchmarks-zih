#!/bin/bash

#SBATCH --account=p_scads

source "./env.sh"

# Check for alpha centauri like partitions where multithreading is enabled by default
if [ "$SLURM_CPUS_PER_TASK" != "$SLURM_CPUS_ON_NODE" ]; then
    echo "[Error] \$SLURM_CPUS_PER_TASK and \$SLURM_CPUS_ON_NODE values are not equal."
    exit 0
fi

# Normal run
slots_per_task=$SLURM_CPUS_ON_NODE
parallelism=$(($SLURM_JOB_NUM_NODES*$SLURM_CPUS_ON_NODE))
source ./benchmark_nexmark.sh run $1 $slots_per_tasks $parallelism
sleep 30s

# Testing for multiple parallelism for given cpus num
#p_start=1
#p_end=$SLURM_CPUS_ON_NODE
#for (( p=p_start; p<=$p_end; p++ )); do
#    parallelism=$(($p*$SLURM_JOB_NUM_NODES))
#    slots_per_task=$p
#    for i in {1..3}; do
#        source ./benchmark_nexmark.sh run $1 $slots_per_task $parallelism
#        sleep 30s
#    done
#done
