#!/bin/bash

# Note:
# Currently ony Apache Flink is suported for nemark benchmarking. In the future, this script
# can be extended to Apache Spark.

function print_nodelist {
    scontrol show hostname $SLURM_NODELIST
}

help_ () {
cat << EOF
[Usage]: source $0 (run [query] [#slots per taskmanager] [parallelism]|setup|start|stop)

[Description]
run   : Run the query. Runs full workflow (setting up configuration files, starting cluster,
        metric collection, run query and stopping cluster)
        query              q[0-22]   : query
        slots/taskmanager  [INTEGER] : number of slots/taskmanager. Usuaslly
                                       it is equal to cpus per device
        parallelism        [INTEGER] : parallelism value. It is equal to
                                       total number of cpus in cluster
setup : Setup configuration files.
start : Setup config files and start cluster and metric collection. It doen't run the query.
        "run_query.sh [query]" should be used to run the query.
stop  : Stop cluster and metric collection.
EOF
}


if [[ "$1" != "run" && "$1" != "start" && "$1" != "stop" && "$1" != "setup" ]] || [[ "$1" == "help" ]] || [[ "$1" == "--help" ]]; then
    help_
fi

if [[ "$1" == "run" ]] || [[ "$1" == "start" ]] || [[ "$1" == "setup" ]]; then
cat << EOF
================================================================================
echo "Started at: `date`" 
Running for config:
QUERY=$2
PARTITION=$SLURM_JOB_PARTITION
NODES=$SLURM_JOB_NUM_NODES
TOTAL_CPUS_PER_NODE=$SLURM_CPUS_ON_NODE
TOTAL_CPUS=$(($SLURM_JOB_NUM_NODES*$SLURM_CPUS_ON_NODE)) [${SLURM_JOB_CPUS_PER_NODE}]
MEM_PER_CPU=`scontrol show job $SLURM_JOBID |grep MinMemoryCPU | sed 's#.*MinMemoryCPU=\([0-9MGmg]*\),*.*$#\1#g'`
TOTAL_MEM=`scontrol show job $SLURM_JOBID |grep mem | sed 's#.*mem=\([0-9MGmg]*\),*.*$#\1#g'`
SLOTS_PER_TASKMANAGER=$3
PARALLELISM=$4
================================================================================
scontrol detailed info
--------------------------------------------------------------------------------
`scontrol show job $SLURM_JOBID`
================================================================================
EOF
    
    # Initialize Java
    ml Java/1.8.0_161-OpenJDK
    
    #set basic environment variables
    source env.sh
   
    # Creating directories
    mkdir -p {$TMP_DIR,$NEXMARK_ROCKSDB_DIR,$NEXMARK_LOG_DIR}
    mkdir -p $TMP_DIR/checkpoints
    chmod g+rwx $TMP_DIR -R
  
    #copy nexmark files to flink
    cp ${NEXMARK_HOME}/conf/sql-client-defaults.yaml ${FLINK_CONF_DIR}/
    cp ${NEXMARK_HOME}/conf/flink-conf.yaml ${FLINK_CONF_DIR}/
    cp ${NEXMARK_HOME}/lib/* ${FLINK_HOME}/lib/
    

    #WORKERS=$(scontrol show hostname $SLURM_NODELIST)   
    MASTER=$( print_nodelist | sort -u | sed -n "1p" )
    WORKERS=$( print_nodelist | sort -u )
    echo "masters:"; echo "$MASTER"
    echo "Workers:"; echo "$WORKERS"    

    # Create/overwrite master file
    echo "$MASTER:8081" > $FLINK_CONF_DIR/masters 

    #create workers file with every available node
    WORKERS_FILE="${FLINK_CONF_DIR}/workers"
    echo "$WORKERS" > $WORKERS_FILE

    #set checkpoints dir
    #sed -i 's#^\(state.checkpoints.dir:\s*\).*$#\1'$NEXMARK_CHECKPOINTS_DIR'#' "${FLINK_HOME}/conf/flink-conf.yaml"
    sed -i 's#^\(state.checkpoints.dir:\s*\).*$#\1'$NEXMARK_CHECKPOINTS_DIR'#' "${FLINK_CONF_DIR}/flink-conf.yaml"

    #set rocksdb
    sed -i 's#^\(state.backend.rocksdb.localdir:\s*\).*$#\1'$NEXMARK_ROCKSDB_DIR'#' "${FLINK_CONF_DIR}/flink-conf.yaml"
    
    #set flink master node 
    sed -i 's#^\(jobmanager.rpc.address:\s*\).*$#\1'$MASTER'#' "${FLINK_CONF_DIR}/flink-conf.yaml"

    #set parallelism equal to toal number of cores available in the cluster
    PARALLELISM=${4:-1}
    sed -i 's#^\(parallelism.default:\s*\).*$#\1'$PARALLELISM'#' "${FLINK_CONF_DIR}/flink-conf.yaml"
    echo "`sed -n 24p ${FLINK_CONF_DIR}/flink-conf.yaml`"

    # set # of slots per task per node(host)
    SLOTS_PER_TASKMANAGER=${3:-1}
    sed -i 's#^\(taskmanager.numberOfTaskSlots:\s*\).*$#\1'$SLOTS_PER_TASKMANAGER'#' "${FLINK_CONF_DIR}/flink-conf.yaml"
    echo "`sed -n 23p ${FLINK_CONF_DIR}/flink-conf.yaml`"

    #set nexmark report node
    sed -i 's#^\(nexmark.metric.reporter.host:\s*\).*$#\1'$MASTER'#' "${NEXMARK_CONF_DIR}/nexmark.yaml"
    sed -i 's#^\(flink.rest.address:\s*\).*$#\1'$MASTER'#' "${NEXMARK_CONF_DIR}/nexmark.yaml"
fi

if [[ "$1" == "run" ]] || [[ "$1" == "start" ]]; then

    #start flink cluster
    start-cluster.sh

    # Adding more taskmanagers (equal to number of cpus per task), one is already started here by start-cluster
    # Note: in below line environment variable is set using source env.sh (not `source <ABSOLUTE_PATH>/env.sh`).
    # For this to work, env.sh file and benchmark_nexmark.sh file should be in the same location. The `<ABSOLUTE_PATH>/env.sh`
    # method is not working
    #for node in $WORKERS; do
    #    #ssh -n $node "source "${BENCHMARK_ROOT_DIR}"/env.sh; for i in {1.."$(($NCPU-1))"}; do "$FLINK_BIN_DIR"/taskmanager.sh start; done"
    #    ssh -n $node "source env.sh; for i in {1.."$(($NCPU-1))"}; do "$FLINK_BIN_DIR"/taskmanager.sh start; done"
    #done
    
    # Maybe add some lines that ensures TaskManagerRunner(s) are started. On some nodes (Rome),
    # TaskManagerRunner appear as Unknown. Nexmark can't recognize it.
    # for node in $WORKERS; do
    #    TASKMANAGER_STAT=`ssh -f $node 'nohup bash -c "jps | grep TaskManagerRunner | wc -l"'`
    #    echo "Number of TaskManagerRunner on $node: $CPU_METRIC_SENDER_STAT"
    #    if [[ "$CPU_METRIC_SENDER_STAT" == 0 ]]; then
    #        echo "[Error]: TaskManagerRunner process was not started by $FLINK_HOME/bin/start-cluster.sh"
    #        echo "Starting TaskManagerRunner manually"
    #        while [[ "$CPU_METRIC_SENDER_STAT" == 0 ]]; do
    #            #ssh -f $node 'nohup bash -c "source "'$BENCHMARK_ROOT_DIR'"/env.sh; "'$NEXMARK_HOME'"/bin/metric_client.sh start"'
    #            ssh -f $node "source "$BENCHMARK_ROOT_DIR"/env.sh; taskmanager.sh start"
    #            sleep 2s
    #            TASKMANAGER_STAT=`ssh -f $node 'nohup bash -c "jps | grep TaskManagerRunner | wc -l"'`
    #        done
    #    fi  
    #done

    # setup nexmark cluster
    setup_cluster.sh
   
    # This routine is to start CpuMetricSender manually. setup_cluster.sh doesn't start it. In line setup_cluster.sh (115), the $FLINK_HOME 
    # variable is not passed to the nodes. Because of this, in metric_client.sh(36) the program is not invoked properly. Need to figure out
    # how to get the required variable upto this point in nexmark source files.
    for node in $WORKERS; do
        CPU_METRIC_SENDER_STAT=`ssh -f $node 'nohup bash -c "jps | grep CpuMetricSender | wc -l"'`
        if [[ "$CPU_METRIC_SENDER_STAT" == 0 ]]; then
            echo "[Error]: CpuMetricSender process was not started by $NEXMARK_HOME/bin/setup_cluster.sh"
            echo "Starting CpuMetricSender process manually"
            while [[ "$CPU_METRIC_SENDER_STAT" == 0 ]]; do
                #ssh -f $node 'nohup bash -c "source "'$BENCHMARK_ROOT_DIR'"/env.sh; "'$NEXMARK_HOME'"/bin/metric_client.sh start"'
                ssh -f $node "source "$BENCHMARK_ROOT_DIR"/env.sh; metric_client.sh start"
                sleep 2s
                CPU_METRIC_SENDER_STAT=`ssh -f $node 'nohup bash -c "jps | grep CpuMetricSender | wc -l"'`
            done
        fi
    done
    for node in $WORKERS; do
        CPU_METRIC_SENDER_STAT=`ssh -f $node 'nohup bash -c "jps | grep CpuMetricSender | wc -l"'`
        echo "Number of CpuMetricSender on $node: $CPU_METRIC_SENDER_STAT"
    done
fi

if [[ "$1" == "run" ]]; then
    #run queries
    run_query.sh $2
fi

if [[ "$1" == "run" ]] || [[ "$1" == "stop" ]]; then
    
    if [[ "$1" == "stop" ]]; then
        #set basic environment variables
        source env.sh
    fi

    #stop nexmark
    shutdown_cluster.sh
    
    #for node in $WORKERS
    for node in $WORKERS; do
        ssh $node "source env.sh; ${FLINK_BIN_DIR}/taskmanager.sh stop-all"
    done

    #stop flink cluster
    stop-cluster.sh

    cat << EOF
Job completed. Ended at: `date`
================================================================================
EOF
fi
