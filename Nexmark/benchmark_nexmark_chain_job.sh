#!/bin/bash
# This script creates chain jobs of nexmark benchmark
# Usage: ./chain_job.sh

###############################################################################
# User input
###############################################################################

# exp1
#PARTITION="alpha"
#NNODES=1
#NCPU="8 12 16 20 24 30 48"
#MEM_PER_CPU="2G"
#TIME="02:00:00"
#QUERIES="q0 q1 q2 q4 q5 q7 q8 q9 q10 q11 q12 q14 q15 q16 q17 q18 q19 q20 q21 q22"

# Scale-out experiments
#PARTITION="haswell64"
#NNODES="1 2 4 8 16 32"
#NCPU="10"
#MEM_PER_CPU="2G"
#TIME="02:00:00"  # Time of each benchmark query run
#QUERIES="q0 q1 q2 q3 q4 q5 q7 q8 q9 q10 q11 q12"

#Scale-up experiments
PARTITION="haswell64"
NNODES="1"
NCPU="8 12 20 24"
MEM_PER_CPU="2G"
TIME="03:00:00"  # Time of each benchmark query run
QUERIES="q0"

#PARTITION="haswell64"
#NNODES="1"
#NCPU="8"
#MEM_PER_CPU="2G"
#TIME="02:00:00"  # Time of each benchmark query run
#QUERIES="q0"

###############################################################################

# Saving chain job commands to a file for reference
CHAIN_JOB_HISTORY="chain_job_history_`date +%s`"
touch "./$CHAIN_JOB_HISTORY"
echo "" > $CHAIN_JOB_HISTORY

SLURM_OUTPUT_DIR=${1:-"./reports"}
mkdir -p $SLURM_OUTPUT_DIR

JOB_FILE="./benchmark_nexmark_sbatch.sh"

DEPENDENCY=""
SLURM_OTHER_CMD=${1:-""}
#SLURM_OTHER_CMD="${SLURM_OTHER_CMD} --reservation=p_scads_476" # reservation

for NUM_NODES in $NNODES; do
    for NUM_CPUS in $NCPU ; do
        # Adding an extrax cpu for buffer and for running the benchmark_nexmark.sh script process.
        for QUERY in $QUERIES; do
            #JOB_FILE_CMD="$QUERY $NUM_CPUS $NUM_CPUS"
            JOB_FILE_CMD="$QUERY"

            TIMESTAMP=`date +%s`
            #OUTPUT_FILE="$SLURM_OUTPUT_DIR/${QUERY}_${NUM_NODES}_${NUM_CPUS}_${PARTITION}_${TIMESTAMP}.out"
            OUTPUT_FILE="$SLURM_OUTPUT_DIR/${QUERY}_${NUM_NODES}_${NUM_CPUS}_${PARTITION}_%j.out"
            
            SLURM_CMD="sbatch"
            MY_JOB_NAME="${QUERY}_${NUM_NODES}_${NUM_CPUS}_${PARTITION}_${MEM_PER_CPU}"
            SLURM_CMD_OPT="--partition=${PARTITION} --nodes=${NUM_NODES} --ntasks=${NUM_NODES} --mem-per-cpu=${MEM_PER_CPU} --cpus-per-task=${NUM_CPUS} --output=${OUTPUT_FILE} --time=${TIME} --job-name=${MY_JOB_NAME}"
                        
            # For slurm partition alpha where # of CPUs appears as # CPUs *2. Enabling
            # multithreading helps to get exact number of cpus (# CPUs on node * #Task),
            # here #CPUs*1=#CPUs
            if [ "${PARTITION}" == "alpha" ]; then
                SLURM_CMD_OPT="${SLURM_CMD_OPT} --hint=multithread"
            fi
            
            # Adding dependency
            if [ -n "${DEPENDENCY}" ] ; then
                SLURM_CMD_OPT="${SLURM_CMD_OPT} --dependency afterany:${DEPENDENCY}"
            fi
           
            # Final slurm command 
            SLURM_CMD="$SLURM_CMD $SLURM_CMD_OPT $SLURM_OTHER_CMD"
            
            # Final command
            JOB_CMD="$SLURM_CMD $JOB_FILE $JOB_FILE_CMD"
            echo -n "Running command: $JOB_CMD"
            echo "${JOB_CMD}" >> $CHAIN_JOB_HISTORY
    
            OUT=`$JOB_CMD`
            echo ""
            echo "$OUT"
            DEPENDENCY=`echo $OUT | awk '{print $4}'`
            sleep 1s
        done
    done
done
